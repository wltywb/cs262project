// HW05
//
// WARNING: This assignment requires the opencv_contrib libraries for SIFT!!!!
// 
//
// CURRENT TO DO: 
//
//	PROBLEM: Gru wins every time. wtf I dont get it.
//
//	SOLUTION!: SIZE OF MATCHES VECTOR ALWAYS = SIZE OF KEYPOINTS
//		-- gonna try applying the thresh thing first
//	IT FUCKING WORKED!!!!!! -- now need to clean up old code and try with more examples
//	I AM SO HAPPY!!!!
//
//		We have 2 things we can work on next: 
//			1) Add a large number of memes that can be looked for + text descriptions for each
//			2) Work on ability to read text from the memes
//
//		1)	A. Create a vector with meme images and test using that to identify the meme in an image
//				- Need to look up how to hard code in the filenames of the example memes
//				- Need to look up how to iterate through a vector
//			B. Create a vector with text descriptions and test outputting correct discription
//
//
// CS262 HW05
// Name:
// Date:

#include "stdafx.h"
#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <string>
#include <vector>
#include <iostream>
#include <string>
#include <vector>
#include <iostream>

using namespace cv;
using namespace std;

void onTrackbar(int value, void* data) {}

int main(int argc, char* argv[])
{

	if (argc < 3)
	{
		//example arguments: ruby.jpg basket.jpg
		// first argument: examplar image
		// second argument: test image
		cout << "Please provide a filename of an exemplar image and a testing image" << endl;
		return 0;
	}

	Mat displayImage;
	Mat displayImage2;

	//read in the images
	// NOTE: image1 = meme1, image2 = test image, image3 = meme2
	Mat image1, image2, image3;
	image1 = imread(argv[1]);
	image2 = imread(argv[2]);
	image3 = imread(argv[3]);
	Mat grayImage1, grayImage2, grayImage3;
	cvtColor(image1, grayImage1, COLOR_BGR2GRAY);
	cvtColor(image2, grayImage2, COLOR_BGR2GRAY);
	cvtColor(image3, grayImage3, COLOR_BGR2GRAY);


	//Use SIFT to get keypoints for both the example image -EI- and the image to search -ITS-
	Ptr<Feature2D> detector = xfeatures2d::SIFT::create();
	vector<KeyPoint> keypoints1;
	vector<KeyPoint> keypoints2;
	vector<KeyPoint> keypoints3;
	detector->detect(grayImage1, keypoints1);
	detector->detect(grayImage2, keypoints2);
	detector->detect(grayImage3, keypoints3);

	//convert the keypoints to descriptors
	// http://docs.opencv.org/modules/features2d/doc/common_interfaces_of_descriptor_extractors.html
	Mat descriptors1;
	Mat descriptors2;
	Mat descriptors3;
	detector->compute(image1, keypoints1, descriptors1);
	detector->compute(image2, keypoints2, descriptors2);
	detector->compute(image3, keypoints3, descriptors3);

	//compare descriptors for both images - resulting in a vector of matches. 
	//***Could use number of matches to decide which example image it is?***
	//http://docs.opencv.org/modules/features2d/doc/common_interfaces_of_descriptor_matchers.html#bfmatcher
	BFMatcher matchmaker;
	vector<DMatch> matches;
	vector<DMatch> matches2;
	//match meme1 (descriptors1) and image (descriptors2)
	matchmaker.match(descriptors1, descriptors2, matches);
	//match meme2 (descriptors3) and image (descriptors2)
	matchmaker.match(descriptors3, descriptors2, matches2);





	//--------WARNING: NEW CODE THAT ELLA WROTE, PROCEED WITH CAUTION----------
	int size1 = matches.size();
	int size2 = matches2.size();

	vector<KeyPoint> betterKeypoints;
	vector<DMatch> betterMatches;
	Mat betterMeme;

	//the following comented out code was used for debugging purposes
	cout << size1 << endl;
	cout << size2 << endl;

	cout << keypoints1.size() << endl;
	cout << keypoints3.size() << endl;


	//instead of deciding which is better image based on size of matches, base it off of percent of keypoints that match 
	double perc1 = (double)size1 / (double)keypoints1.size();
	double perc2 = (double)size2 / (double)keypoints3.size();

	cout << perc1 << endl;
	cout << perc2 << endl;

	//determine which one is bigger
	if (perc1 > perc2) {
		betterKeypoints = keypoints1;
		betterMatches = matches;
		image1.copyTo(betterMeme);
	}
	else {
		betterKeypoints = keypoints3;
		betterMatches = matches2;
		image3.copyTo(betterMeme);
	}

	//---------END OF CODE WRITTEN BY ELLA-----------

	int maxVal = 500;
	int threshold = 150;
	int prevThreshold = 0;

	bool notSavedYet = true;

	namedWindow("Image Window", 1);
	createTrackbar("Match Thresh", "Image Window", &threshold, maxVal);

	while (1 == 1)
	{
		if (threshold != prevThreshold)
		{
			// EDITS: threshold both sets of matches then compare size

			vector<DMatch> threshMatches1;
			vector<DMatch> threshMatches2;

			for (int i = 0; i < matches.size(); i++) {
				if (matches[i].distance < threshold) { threshMatches1.push_back(matches[i]); }
			}
			for (int i = 0; i < matches2.size(); i++) {
				if (matches2[i].distance < threshold) { threshMatches2.push_back(matches2[i]); }
			}

			//determine which one is bigger
			if (threshMatches1.size() > threshMatches2.size()) {
				betterKeypoints = keypoints1;
				betterMatches = matches;
				image1.copyTo(betterMeme);
			}
			else {
				betterKeypoints = keypoints3;
				betterMatches = matches2;
				image3.copyTo(betterMeme);
			}
			//vector<DMatch> finalMatches;
			//for (int i = 0; i<betterMatches.size(); i++)
			//{
			//if (betterMatches[i].distance < threshold)
			//{
			//finalMatches.push_back(betterMatches[i]);
			//}



			//}

			//http://docs.opencv.org/modules/features2d/doc/drawing_function_of_keypoints_and_matches.html?
			drawMatches(betterMeme, betterKeypoints, image2, keypoints2, betterMatches, displayImage);
			//drawMatches(image3, keypoints3, image2, keypoints2, matches2, displayImage2);
			prevThreshold = threshold;
		}

		//the following comented out code was used for debugging purposes
		imshow("image1", image1);
		imshow("image2", image2);
		imshow("image3", image3);
		imshow("betterMeme", betterMeme);
		imshow("Image Window", displayImage);
		char key = waitKey(33);
		if (key == 'q')
		{
			break;
		}
		if (key == ' ')
		{
			imwrite("matchingYourObject.png", displayImage);

		}
	}
}