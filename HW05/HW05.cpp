// HW05
//
// WARNING: This assignment requires the opencv_contrib libraries for SIFT!!!!
// 
//
// CURRENT TO DO: 
//
//	PROBLEM: Gru wins every time. wtf I dont get it.
//
//	SOLUTION!: SIZE OF MATCHES VECTOR ALWAYS = SIZE OF KEYPOINTS
//		-- gonna try applying the thresh thing first
//	IT FUCKING WORKED!!!!!! -- now need to clean up old code and try with more examples
//	I AM SO HAPPY!!!!
//
//		We have 2 things we can work on next: 
//			1) Add a large number of memes that can be looked for + text descriptions for each
//			2) Work on ability to read text from the memes
//
//		1)	A. Create a vector with meme images and test using that to identify the meme in an image
//				- Need to look up how to hard code in the filenames of the example memes
//				- Need to look up how to iterate through a vector
//			B. Create a vector with text descriptions and test outputting correct discription
//
//
// CS262 HW05
// Name:
// Date:

#include "stdafx.h"
#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <string>
#include <vector>
#include <iostream>
#include <string>
#include <vector>
#include <iostream>

using namespace cv;
using namespace std;

void onTrackbar(int value, void* data) {}

vector<Mat> initializeMemes() {
	vector<Mat> images;

	Mat image = imread("gruBlank.jpg");
	images.push_back(image);

	image = imread("perish.jpg");
	images.push_back(image);

	image = imread("drake.jpg");
	images.push_back(image);

	image = imread("carSalesman.jpg");
	images.push_back(image);

	image = imread("pigeon.jpg");
	images.push_back(image);

	image = imread("pikachu.jpg");
	images.push_back(image);

	return images;
}

vector<string> initializeCaptions() {
	vector<string> captions;

	string cap = "Gru's Plan is a four panel comic where a character outlines a plan with an unexpected third step.";
	captions.push_back(cap);

	cap = "A red filtered image of former president Barack Obama's eyes, usually accompanied by the phrase 'then perish'";
	captions.push_back(cap);

	cap = "Two images of the musician Drake. One in which he looks disgusted and another in which he looks appreciative";
	captions.push_back(cap);

	cap = "An illustration of a car salesman and the buyer, usually accompanied by the phrase 'this bad boy can fit so much X in it'";
	captions.push_back(cap);

	cap = "An anime character thinks a butterfly is a pigeon. Used to express confusion";
	captions.push_back(cap);

	cap = "Image of the pokemon character pikachu looking surprised. Often used in response to a predictable outcome that nevertheless leaves one surprised";
	captions.push_back(cap);

	return captions;
}

vector<Mat> convertToGray(vector<Mat> images) {
	//get size of image vector
	int size = images.size();

	//create mat for gray images
	vector<Mat> grayImages;

	//Mat to hold gray image
	Mat grayImg;

	//iterate through images and convert to grayscale
	for (int i = 0; i < size; i++) {
		cvtColor(images[i], grayImg, COLOR_BGR2GRAY);
		grayImages.push_back(grayImg);
	}

	return grayImages;
}

vector<Mat> computeDescriptors(vector<Mat> images, vector<Mat> grayImages) {
	//Use SIFT to get keypoints then convert keypoints to descriptors
	Ptr<Feature2D> detector = xfeatures2d::SIFT::create();

	//vector to hold keypoints
	vector<KeyPoint> keypoints;
	
	//Mat to hold descriptors
	Mat descriptors;

	//size of input
	int size = images.size();

	//vector to hold descriptors
	vector<Mat> allDescriptors;

	//iterate through images, getting keypoints then converting them to descriptors
	for (int i = 0; i < size; i++) {
		detector->detect(grayImages[i], keypoints);
		detector->compute(images[i], keypoints, descriptors);

		allDescriptors.push_back(descriptors);
	}

	return allDescriptors;
}

int getBestMeme(Mat inputDescript, vector<Mat> memeDescripts, int threshold) {
	BFMatcher matchmaker;

	//largest number of matches seen
	int mostMatches = 0;

	//index of meme with most matches
	int bestMatch = 0;

	//size of meme vect
	int size = memeDescripts.size();

	//size of matches
	int matchSize;

	//vector to hold matches
	vector<DMatch> matches;

	//vector to hold thresholded matches
	vector<DMatch> threshMatches;

	for (int i = 0; i < size; i++) {
		//get matches
		matchmaker.match(memeDescripts[i], inputDescript, matches);

		//get size of matches
		matchSize = matches.size();

		//thresh matches
		for (int j = 0; j < matchSize; j++) {
			if (matches[i].distance < threshold) { threshMatches.push_back(matches[i]); }
		}

		//check if has more matches than current best
		if (threshMatches.size() > mostMatches) {
			mostMatches = threshMatches.size();
			bestMatch = i;
		}

		matches.empty();
	}

	return bestMatch;
}

int main(int argc, char* argv[])
{


Mat displayImage;

//read in the images
Mat input;
input = imread(argv[1]);
//convert to grayscale
Mat grayinput;
cvtColor(input, grayinput, COLOR_BGR2GRAY);

// initialize a vector of all of the memes
vector<Mat> memes = initializeMemes();

//initialize captions
vector<string> captions = initializeCaptions();

//get all the memes in grayscale
vector<Mat> grayMemes = convertToGray(memes);

//Use SIFT to get keypoints for both the example image -EI- and the image to search -ITS-
Ptr<Feature2D> detector = xfeatures2d::SIFT::create();
vector<KeyPoint> inputKeypoints;
detector->detect(grayinput, inputKeypoints);

//convert the keypoints to descriptors
// http://docs.opencv.org/modules/features2d/doc/common_interfaces_of_descriptor_extractors.html

Mat inputDescriptors;
detector->compute(input, inputKeypoints, inputDescriptors);

//get all the memes in grayscale
vector<Mat> memeDescripts = computeDescriptors(memes, grayMemes);

//compare descriptors for both images - resulting in a vector of matches. 
//***Could use number of matches to decide which example image it is?***
//http://docs.opencv.org/modules/features2d/doc/common_interfaces_of_descriptor_matchers.html#bfmatcher
BFMatcher matchmaker;

//to hold the best meme info
vector<KeyPoint> betterKeypoints;
vector<DMatch> betterMatches;
Mat betterMeme;

int bestMeme;

//info for while loop
int maxVal = 500;
int threshold = 150;
int prevThreshold = 0;

namedWindow("Image Window", 1);
createTrackbar("Match Thresh", "Image Window", &threshold, maxVal);

while (1 == 1)
{
	if (threshold != prevThreshold)
	{

		bestMeme = getBestMeme(inputDescriptors, memeDescripts, threshold);
		cout << bestMeme << endl;

		betterMeme = memes[bestMeme];

		matchmaker.match(memeDescripts[bestMeme], inputDescriptors, betterMatches);

		detector->detect(grayMemes[bestMeme], betterKeypoints);
		
		cout << captions[bestMeme] << endl;

		//http://docs.opencv.org/modules/features2d/doc/drawing_function_of_keypoints_and_matches.html?
		drawMatches(betterMeme, betterKeypoints, input, inputKeypoints, betterMatches, displayImage);
		prevThreshold = threshold;
	}

		imshow("betterMeme", betterMeme);
		imshow("Image Window", displayImage);
		char key = waitKey(33);
		if (key == 'q')
		{
			break;
		}
		if (key == ' ')
		{
			imwrite("matchingYourObject.png", displayImage);
			
		}
	}
}



