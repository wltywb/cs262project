// OpenCV SIFT/SURF Feature Detector
//
// NOTE/WARNING: This demo relies on libraries in the opencv_contrib branch.
// which is not included with the default OpenCV install
//
// Specifically, the xfeatures2d include files and lib/dll files.
//


#include "stdafx.h"
#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <string>
#include <vector>
#include <iostream>

using namespace cv;
using namespace std;

Mat originalImage;
Mat displayImage;

int main(int argc, char* argv[])
{

	if (argc <= 1)
	{
		cout << "Please provide a filename of an image" << endl;
		return 0;
	}

	//read in the image
	originalImage = imread(argv[1]);
	Mat grayImage;
	cvtColor(originalImage, grayImage, COLOR_BGR2GRAY);

	//SiftFeatureDetector detector;
	Ptr<xfeatures2d::SIFT> detector = xfeatures2d::SIFT::create();
	//Ptr<Feature2D> detector = xfeatures2d::SURF::create();
	//Ptr<Feature2D> detector = ORB::create();
	// you get the picture, i hope..
	vector<KeyPoint> keypoints;
	detector->detect(grayImage, keypoints);

	// Add results to image and save.
	drawKeypoints(originalImage, keypoints, displayImage, Scalar::all(-1), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

	while (1 == 1)
	{
		imshow("Image Window", displayImage);
		char key = waitKey(33);
		if (key == 'q')
		{
			break;
		}
		if (key == ' ')
		{
		}
	}
}
